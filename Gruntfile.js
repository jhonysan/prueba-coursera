module.exports = function(grunt){
	require('time-grunt')(grunt);
	require('jit-grunt')(grunt,{
		useminPrepare: 'grunt-usemin'
	});
	
	grunt.initConfig({
		sass: {
			dist: {
				files: [{
					expand: true,
					cwd: 'css',
					src: ['*.scss'],
					dest: 'css',
					ext: '.css'
				}]
			}
		},
		
		watch: {
			files: ['css/*.scss'],
			tasks: ['css']
		},
		
		imagemin: {
			dynamic: {
				files: [{
					expand:true,
					cwd: './',
					src: 'img/*.{png,gif,jpg,jpeg}',
					dest: 'dist/'
				}]
			}
		},
	});
	
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.registerTask('img:compress', ['imagemin']);
	grunt.registerTask('css',['sass']);
};